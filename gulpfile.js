const   gulp         = require('gulp'),
        sass         = require('gulp-sass'),
        cleanCSS     = require('gulp-clean-css'),
        autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function() {

    gulp.src('./dist/sass/style.scss')

        .pipe(sass({outputStyle: 'compressed'}))

        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))

        .pipe(gulp.dest('./dist/css'));

});


gulp.task('default',['sass'], function() {

    gulp.watch('./dist/sass/**/*.scss', ['sass']);

});
