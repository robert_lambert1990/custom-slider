$(document).ready(function(){
	$(window).resize(function(){
		if($('.overlayingimg').css('top') == "-55px") {
			var width = $('html').width();
			var left = width / 50;
			left = left - 28;
			console.log(left);
			$('.overlayingimg').css('left', left+'%');
		}
		else {
			$('.overlayingimg').css('left', '');
		}
	});


		$('.activesides .slider').swipe( {
			swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
				if(direction == 'left' || direction == 'right') {
	      			$('.pagintation li.active img').attr('src', 'dist/images/pagination_03.png');
					$('.pagintation li.active').removeClass('active');

					var value = $(this).index();
					value++;				
					var oldActive = $(this);
					var newActive;			
					var notActive;	
					var value2;
		    	
		    		if(direction == 'left') {	    	
		    			if(value == 1) {
		    				newActive = $('.slider:nth-child(3)');
		    				notActive = $('.slider:nth-child(2)');
		    				value2 = 3;
		    			} else if(value == 2) {
		    				newActive = $('.slider:nth-child(1)');
		    				notActive = $('.slider:nth-child(3)');
		    				value2 = 1;
		    			} else {
		    				newActive = $('.slider:nth-child(2)');
		    				notActive = $('.slider:nth-child(1)');
		    				value2 = 2;
		    			}
		    		} else {
		    			if(value == 3) {
		    				newActive = $('.slider:nth-child(1)');
		    				notActive = $('.slider:nth-child(2)');
		    				value2 = 1;
		    			} else if(value == 2) {
		    				newActive = $('.slider:nth-child(3)');
		    				notActive = $('.slider:nth-child(1)');
		    				value2 = 3;
		    			} else {
		    				newActive = $('.slider:nth-child(2)');
		    				notActive = $('.slider:nth-child(3)');
		    				value2 = 2;
		    			}
		    		}	    		

					$('.pagintation li:nth-child(' + value2 + ')').find('img').attr('src', 'dist/images/active.png');
					$('.pagintation li:nth-child(' + value2 + ')').addClass('active');

		    		oldActive.removeClass('active');
		    		notActive.removeClass('active');
		    		newActive.addClass('active');

		    		$(oldActive).find($('.indiv-img')).animate({  left:'+=50%'}, 350, function(){
						$(oldActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {	
							$(newActive).animate({ left: '0%'}, 0, function() {
								$(oldActive).animate({ left: '-20%'}, 220, function() {
									$(notActive).animate({ left: '200%'}, 250, function() {
										$(oldActive).animate({ left: '200%'}, 250, function() {
											$(newActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {				
												$(newActive).find($('.indiv-img')).animate({ left:'-=50%'}, 300, function(){
													$('body').css('overflow-x', 'visible');		
												});
											});
										});
									});
								}); 
						}); 								
					});
				});
	    	}
	    }
	});

	$('.pagintation li').on('click', function(e){
		e.preventDefault();
		
		if(!$(this).hasClass('active')) {
			
			$('.pagintation li.active img').attr('src', 'dist/images/pagination_03.png');
			$('.pagintation li.active').removeClass('active');

			$(this).addClass('active');
			$(this).find('img').attr('src', 'dist/images/active.png');
			var value = $(this).index();
			var newActive;
			var oldActive;
			var notActive;

			$('.sliders .activesides .slider').each(function(){
				var value2 = $(this).index();
				if(value2 == value) {
					newActive = $(this);
					$(this).addClass('active');							
				}
				else {			
					if($(this).hasClass('active')) {
						oldActive = $(this);
						$(this).removeClass('active');
					}		
					else {
						notActive = $(this);
						$(this).removeClass('active');
					}									
				}
			});
			
			$(oldActive).find($('.indiv-img')).animate({  left:'+=50%'}, 350, function(){
				$(oldActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {	
					$(newActive).animate({ left: '0%'}, 0, function() {
						$(oldActive).animate({ left: '-20%'}, 220, function() {
							$(notActive).animate({ left: '200%'}, 250, function() {
								$(oldActive).animate({ left: '200%'}, 250, function() {
									$(newActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {				
										$(newActive).find($('.indiv-img')).animate({ left:'-=50%'}, 300, function(){
											$('body').css('overflow-x', 'visible');		
										});
									});
								});
							});
						}); 
					}); 								
				});
			});
		};
	});

	setTimeout(function(){
		(function(){
		    automaticSlider();
		    setTimeout(arguments.callee, 10000);
		})();
	}, 10000);

	function automaticSlider() {

		var oldActive = $('.slider.active');
		var value = $(oldActive).index();	
		value++;
		var value2;

			$('.pagintation li.active img').attr('src', 'dist/images/pagination_03.png');
			$('.pagintation li.active').removeClass('active');

			if(value == 3) {
				newActive = $('.slider:nth-child(1)');
				notActive = $('.slider:nth-child(2)');
				value2 = 1;
			} else if(value == 2) {
				newActive = $('.slider:nth-child(3)');
				notActive = $('.slider:nth-child(1)');
				value2 = 3;
			} else {
				newActive = $('.slider:nth-child(2)');
				notActive = $('.slider:nth-child(3)');
				value2 = 2;
			}

			oldActive.removeClass('active');
		    notActive.removeClass('active');
		    newActive.addClass('active');

		    $('.pagintation li:nth-child(' + value2 + ')').find('img').attr('src', 'dist/images/active.png');
			$('.pagintation li:nth-child(' + value2 + ')').addClass('active');
		    			    		
			
			$(oldActive).find($('.indiv-img')).animate({  left:'+=50%'}, 350, function(){
				$(oldActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {	
					$(newActive).animate({ left: '0%'}, 0, function() {
						$(oldActive).animate({ left: '-20%'}, 220, function() {
							$(notActive).animate({ left: '200%'}, 250, function() {
								$(oldActive).animate({ left: '200%'}, 250, function() {
									$(newActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {				
										$(newActive).find($('.indiv-img')).animate({ left:'-=50%'}, 300, function(){
											$('body').css('overflow-x', 'visible');		
										});
									});
								});
							});
						}); 
					}); 								
				});
			});
		};


});