$(document).ready(function(){
	$(window).resize(function(){
		if($('.overlayingimg').css('top') == "-55px") {
			var width = $('html').width();
			var left = width / 50;
			left = left - 28;
			$('.overlayingimg').css('left', left+'%');
		}
		else {
			$('.overlayingimg').css('left', '');
		}
	});


	$('.activesides .slider').swipe( {
		swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
				
			if(direction == 'left' || direction == 'right') {	      			

				var sliderIndex = $(this).index() + 1;				
				var oldActive = $(this);
				var newActive;			
				var notActive;	
				var paginationIndex;
	    	
	    		if(direction == 'left') {	    	
	    			if(sliderIndex == 1) {		    			
	    				newActive = sliderAssignment(3);
	    				notActive = sliderAssignment(2);
	    				paginationIndex = 3;
	    			} else if(sliderIndex == 2) {
	    				newActive = sliderAssignment(1);
	    				notActive = sliderAssignment(3);
	    				paginationIndex = 1;
	    			} else {
	    				newActive = sliderAssignment(2);
	    				notActive = sliderAssignment(1);
	    				paginationIndex = 2;
	    			}
	    		} else {
	    			if(sliderIndex == 3) {
	    				newActive = sliderAssignment(1);
	    				notActive = sliderAssignment(2);
	    				paginationIndex = 1;
	    			} else if(sliderIndex == 2) {
	    				newActive = sliderAssignment(3);
	    				notActive = sliderAssignment(1);
	    				paginationIndex = 3;
	    			} else {
	    				newActive = sliderAssignment(2);
	    				notActive = sliderAssignment(3);
	    				paginationIndex = 2;
	    			}
	    		}	  
			
	    		classCleanUp(paginationIndex, oldActive, notActive, newActive);
	   			sliderAnimation(oldActive, notActive, newActive);
	    	}
	    }
	});

	$('.pagintation li').on('click', function(e){
		e.preventDefault();	
		
		if(!$(this).hasClass('active')) {
			
			$('.pagintation li.active img').attr('src', 'dist/images/pagination_03.png');
			$('.pagintation li.active').removeClass('active');

			$(this).addClass('active');
			$(this).find('img').attr('src', 'dist/images/active.png');
			var value = $(this).index();
			var newActive;
			var oldActive;
			var notActive;

			$('.sliders .activesides .slider').each(function(){
				var value2 = $(this).index();
				if(value2 == value) {
					newActive = $(this);
					$(this).addClass('active');							
				}
				else {			
					if($(this).hasClass('active')) {
						oldActive = $(this);
						$(this).removeClass('active');
					}		
					else {
						notActive = $(this);
						$(this).removeClass('active');
					}									
				}
			});
			
			sliderAnimation(oldActive, notActive, newActive);
		};
	});


	var timer = new Timer(function() {
	    automaticSlider();
	}, 10000);
	
	function automaticSlider() {

		var oldActive = $('.slider.active');
		var sliderIndex = $(oldActive).index() + 1;	
		var paginationIndex;

		if(sliderIndex == 3) {
			newActive = sliderAssignment(1);
			notActive = sliderAssignment(2);
			paginationIndex = 1;
		} else if(sliderIndex == 2) {
			newActive = sliderAssignment(3);
			notActive = sliderAssignment(1);
			paginationIndex = 3;
		} else {
			newActive = sliderAssignment(2);
			notActive = sliderAssignment(3);
			paginationIndex = 2;
		}

		oldActive.removeClass('active');
	    notActive.removeClass('active');
	    newActive.addClass('active');

	    classCleanUp(paginationIndex, oldActive, notActive, newActive);
	   	sliderAnimation(oldActive, notActive, newActive);
	};


	function sliderAnimation(oldActive, notActive, newActive) {
		timer.reset(10000);
 		$(oldActive).find($('.indiv-img')).animate({  left:'+=50%'}, 350, function(){
			$(oldActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {	
				$(newActive).animate({ left: '0%'}, 0, function() {
					$(oldActive).animate({ left: '-20%'}, 220, function() {
						$(notActive).animate({ left: '200%'}, 250, function() {
							$(oldActive).animate({ left: '200%'}, 250, function() {
								$(newActive).find($('.indiv-img')).animate({  width:'toggle'}, 0, function() {				
									$(newActive).find($('.indiv-img')).animate({ left:'-=50%'}, 300, function(){
										$('body').css('overflow-x', 'visible');		
									});
								});
							});
						});
					}); 
				}); 								
			});
		});
	}

	function classCleanUp(paginationIndex, oldActive, notActive, newActive) {
		$('.pagintation li.active img').attr('src', 'dist/images/pagination_03.png');
		$('.pagintation li.active').removeClass('active');
		$('.pagintation li:nth-child(' + paginationIndex + ')').find('img').attr('src', 'dist/images/active.png');
		$('.pagintation li:nth-child(' + paginationIndex + ')').addClass('active');
		oldActive.removeClass('active');
		notActive.removeClass('active');
		newActive.addClass('active');
	}

	function sliderAssignment(int) {
		return $('.slider:nth-child(' + int + ')');
	}

	function Timer(fn, t) {
	    var timerObj = setInterval(fn, t);

	    this.stop = function() {
	        if (timerObj) {
	            clearInterval(timerObj);
	            timerObj = null;
	        }
	        return this;
	    }

	    this.start = function() {
	        if (!timerObj) {
	            this.stop();
	            timerObj = setInterval(fn, t);
	        }
	        return this;
	    }

	    this.reset = function(newT) {
	        t = newT;
	        return this.stop().start();
	    }
	}


});